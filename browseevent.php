<?php
/*VERSION REPORT STATUS*/
// Start session
session_start();
require_once('./controlpanel/includes/functions.inc.php');
?>
<?php
if(isset($_SESSION['logged_in'])) { //check for login
    include "./controlpanel/includes/config.inc.php";
    $con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    if($con->connect_error){
        die("Connection failed: ".$con->connect_error);
    }
    $op = 0; //get fillter mode
    if(isset($_GET['mode'])) {
        if($_GET['mode']=='showexp') {
            $op = 1;
        } else if($_GET['mode']=='showableregis') {
            $op = 0;
        } else {
            $op = 2;
        }
    }
    $query;
    $modetxt = "";
    $ctime = time();
    $a='';
    $b = '';
    $c='';
    if($op==1) {
        //show expired (all)
        $query = "SELECT * FROM `events`";
        $modetxt = "Show all events";
        $b = '';
        $a = 'selected="selected"';
    } else if ($op==0) {
        //show able to regis
        $query = "SELECT `eventID`, `eventName`, `eventDesc`, `ticketPrice`, `createdOn`, `registerClosed`, `eventStart`, `joined`, `capacity`, `eventAdmin`, `location` FROM `events` INNER JOIN `tickets` ON `events`.`eventID` = `tickets`.`forEvent` WHERE `events`.`registerClosed`>". $ctime ." GROUP BY `eventID` HAVING `events`.`capacity`>COUNT(`tickets`.`forEvent`)";
        $modetxt = "Show only event that can still be able to registers";
        $b = 'selected="selected"';
    } else {
        //show event not started
        $query = "SELECT * FROM `events` WHERE '".$ctime."' < `eventStart`";
        $modetxt = "Show only event that hasn't started yet";
        $c = 'selected="selected"';
        $b = '';
    }
    $result = mysqli_query($con, $query) or die("Data not found.");
    $data = array();
    while($row = mysqli_fetch_assoc($result)) { //construct data array
        $row['createdOn'] = date('d/m/y', $row['createdOn']);
        $row['rawRegClosed'] = $row['registerClosed'];
        $row['registerClosed'] = date('d/m/y', $row['registerClosed']);
        $row['eventStart'] = date('d/m/y', $row['eventStart']);
        $query = "SELECT `username` FROM `users` WHERE userID = '".$row['eventAdmin']."'";
        $result2 = mysqli_query($con, $query) or die("Data not found.");
        $result2 = mysqli_fetch_array($result2);
        $row['eventAdmin'] = $result2['username'];
        $data[] = $row;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Ticket Now</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        <!-- DataTables CSS -->
    <link href="controlpanel/js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="controlpanel/js/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <?php include 'header.php'; constructHeader(__FILE__); ?>
    <div class="container"><?php if(isset($_SESSION['admin'])) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Browse events</h2>
                        <h4>Fillter: <form action="browseevent.php" method="get" accept-charset="utf-8"><select name="mode">
                            <option value="showexp" <?php echo $a; ?>>Show all</option>
                            <option value="showableregis" <?php echo $b; ?>>Able to register</option>
                            <option value="showb4start" <?php echo $c; ?>>Not started yet</option>
                        </select><input type="submit" /></form>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Event ID</th>
                                        <th>Event Name</th>
                                        <th>Register Closed</th>
                                        <th>Event Day</th>
                                        <th>Capacity</th>
                                        <th>Ticket Price</th>
                                        <th>Location</th>
                                        <th>Admin</th>
                                        <th>Join</th>
                                    </tr>
                                </thead>
                                <tbody>
                               <?php 
                                $curr = 0;
                                foreach ($data as $row) {

                                    $query = "SELECT `ticketID` FROM `tickets` WHERE `owner`='".$_SESSION['uid']."' AND `forEvent`='".$row['eventID']."'";
                                    $result3 = mysqli_query($con, $query) or die("Data not found.");
                                    $joined = false;
                                    $query = "SELECT COUNT(`forEvent`) AS joined FROM tickets WHERE forEvent=".$row['eventID'];
                                    $result4 = mysqli_query($con, $query) or die("Data not found.");
                                    $result4 = mysqli_fetch_array($result4);
                                    $row['joined'] = $result4['joined'];
                                    if(mysqli_num_rows($result3)>=1) {
                                        $joined = true;
                                    }
                                    echo '<tr>';
                                    echo '<td>' . $row['eventID'] . '</td>';
                                    ?><td><a href="event_view_fnt.php?q=<?php echo $row['eventID']; ?>"><?php echo $row['eventName']; ?></a></td> <?php
                                    echo '<td>' . $row['registerClosed'] . '</td>';
                                    echo '<td>' . $row['eventStart'] . '</td>';
                                    echo '<td>' . $row['capacity'] . '</td>';
                                    echo '<td>' . $row['ticketPrice'] . '</td>';
                                    echo '<td>' . $row['location'] . '</td>';
                                    echo '<td>' . $row['eventAdmin'] . '</td>';
                                    if($row['joined']<$row['capacity'] && $ctime < $row['rawRegClosed'] && !$joined) {
                                        echo '<td align="center">' . '<a href="event_join_fnt.php?q='. $row['eventID'] . '"><i class="glyphicon glyphicon-edit"></i></a>' . '</td>';
                                    } else {
                                        echo '<td>' . " " . '</td>';
                                    }
                                    echo "</tr>";
                                    }
                                 ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <?php } else {
            include 'not_logged_in.php';
        }  ?>
    </div>
    
    <?php include('footer.php'); ?>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
    <script src="controlpanel/js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="controlpanel/js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
</body>
</html>
