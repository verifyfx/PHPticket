<?php
	function constructHeader($location) { //this will make selected menu became active (old school)
		$location = basename($location);
		$a = "";
		$d = "";
		$e = "";
		$x = 'class="active"';
		switch($location) {
			case "index.php":
				$a = $x;
				break;
			case "browseevent.php":
				$d = $x;
				break;
			case "findticket.php":
				$e = $x;
				break;
		}
		?>

		<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href="http://facebook.com"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.php">
                    	<h1><img src="images/logo.png" alt="logo"></h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php echo $a; ?>><a href="index.php">Home</a></li>
                        <li <?php echo $d; ?> ><a href="browseevent.php">Browse for events</a></li>
                        <li <?php echo $e; ?> ><a href="findticket.php">Find your ticket</a></li>
                        <li><a href="./controlpanel/">Administration</a></li>
                        <?php if(isset($_SESSION['logged_in'])) { ?>
                            <li><a href="controlpanel/logout.php">Log out</a></li>
                        <?php } else { ?>
                            <li><a href="controlpanel/login.php">Login</a></li>  
                        <?php } ?>     
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->
		<?php
	}
?>