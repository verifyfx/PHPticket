<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('controlpanel/includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('controlpanel/login.php');
    }
?>
<?php //check for parameters if not exist send user back
    if(!isset($_GET['q'])) header("location: findticket.php");
    include "./controlpanel/includes/config.inc.php";
    $con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    if($con->connect_error){
        die("Connection failed: ".$con->connect_error);
    }
    $query = "SELECT * FROM `events` WHERE eventID = '".$_GET['q']."'";
    $result = mysqli_query($con, $query) or die("Data not found.");
    $result = mysqli_fetch_array($result);
    $result['createdOn'] = date('d/m/y', $result['createdOn']);
    $result['registerClosedRaw'] = $result['registerClosed'];
    $result['registerClosed'] = date('d/m/y', $result['registerClosed']);
    $result['eventStart'] = date('d/m/y', $result['eventStart']);

    $query = "SELECT `username` FROM `users` WHERE userID = '".$result['eventAdmin']."'";
    $result2 = mysqli_query($con, $query) or die("Data not found.");
    $result2 = mysqli_fetch_array($result2);

    $query = "SELECT `ticketID` FROM `tickets` WHERE `owner`='".$_SESSION['uid']."' AND `forEvent`='".$result['eventID']."'";
    $result3 = mysqli_query($con, $query) or die("Data not found.");
    $joined = false;
    if(mysqli_num_rows($result3)>=1) {
        $joined = true;
    }
    $result3 = mysqli_fetch_array($result3);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Ticket Now</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <?php include 'header.php'; constructHeader(__FILE__); ?>
    <div class="container">
        <div class="row">
                <h3>Event Details</h3>
            <div class="col-lg-8">
                <p>Event ID: <span><strong><?php echo $result['eventID']; ?></strong></span></p>
                <p>Name: <span><strong><?php echo $result['eventName']; ?></strong></span></p>
                <p>Description: <span><strong><?php echo $result['eventDesc']; ?></strong></span></p>
                <p>Creation Time: <span><strong><?php echo $result['createdOn']; ?></strong></span></p>
                <p>Register Closed: <span><strong><?php echo $result['registerClosed']; ?></strong></span></p>
                <p>Event Day: <span><strong><?php echo $result['eventStart']; ?></strong></span></p>
                <p>Capacity: <span><strong><?php echo $result['capacity']; ?></strong></span></p>
                <p>Ticket Price: <span><strong><?php echo $result['ticketPrice']; ?></strong></span></p>
                <p>Event Admin: <span><strong><?php echo $result2['username']; ?></strong></span></p>
            </div>
        </div>
        <div class="row">
                <?php if($joined) {
                    ?><?php if(isset($_GET['add'])) {?>
                        <h3><b>You created a ticket!</b></h3>
                        <?php } else {?>
                            <h3><b>You already have a ticket!</b></h3>
                            <?php } ?>
                <h4>here's the information</h4><?php
                    ?><p>Ticket ID: <span><strong><?php echo $result3['ticketID']; ?></strong></span></p><?php
                 } ?>
        </div>
        <div class="row"><h2>
                        <?php if(isset($_SESSION['logged_in'])) { if($_SESSION['uid']==$result['eventAdmin'] || $_SESSION['admin']) { ?>
            <a href="controlpanel/event_manage.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicon glyphicon-cog"></i>Edit this event</a><br>
            <a href="controlpanel/event_delete.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicon glyphicon-trash"></i><font color="RED">Delete this event</font></a><br> <?php }} ?>
            <?php if(!$joined) { 
                    if(time()<$result['registerClosedRaw']) { ?>
                        <a href="event_join_fnt.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicon glyphicon-ok"></i>Join this event!</a>
                        <br>
            <?php }
                } else {
                ?><a href="event_print_ticket.php?tid=<?php echo $result3['ticketID']; ?>"><i class="glyphicon glyphicon-print"></i>Print the ticket</a><BR><?php
                if(time()<$result['registerClosedRaw']) {
                    ?><a href="event_leave_fnt.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicon glyphicon-remove"></i>Leave this event</a><br><?php
                }
            ?>

            <?php } ?>
            <a href="findticket.php"><i class="glyphicon glyphicon-list-alt"></i>Go to event list</a>
            </h2>
        </div>
        <br><br><br>
    </div>
    
    <?php include('footer.php'); ?>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
