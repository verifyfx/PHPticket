<?php
	//generate random event list
	include "controlpanel/includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$query = "SELECT * FROM `events` WHERE '".time()."' < `registerClosed`";
	$result = mysqli_query($con, $query) or die("Data not found.");
	header("Content-Type: application/json");
	$data = array();
    while($row = mysqli_fetch_assoc($result)) {
    	$row['createdOn'] = date('d/m/y', $row['createdOn']);
    	$row['registerClosed'] = date('d/m/y', $row['registerClosed']);
    	$row['eventStart'] = date('d/m/y', $row['eventStart']);
        $data[] = $row;
    }
    //quick and dirty way to generate 3 random number 55555555+
    $min=0;
	$max=count($data)-1;
	$output = array();
	if($max<3) {
		$num = array();
		$num[0] = rand($min,$max);
		$num[1] = rand($min,$max);
		while($num[0]==$num[1]) { $num[1] = rand($min, $max); }
		$num[2] = rand($min,$max);
		while($num[1]==$num[2] || $num[0]==$num[2] ) { $num[2] = rand($min, $max); }
		$output[0] = $data[$num[0]];
		$output[1] = $data[$num[1]];
		$output[2] = $data[$num[2]];
		echo json_encode($output);
	} else {
		$output[0] =  $data[rand($min,$max)];
		$output[1] =  $data[rand($min,$max)];
		$output[2] =  $data[rand($min,$max)];
	    echo json_encode($output);
	}
?>