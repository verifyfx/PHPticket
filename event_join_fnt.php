<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('controlpanel/includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('controlpanel/login.php');
    }
?>

<?php //check for parameters
if(!isset($_GET['q'])) header("location: browseevent.php");
include "./controlpanel/includes/config.inc.php";
$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
if($con->connect_error){
    die("Connection failed: ".$con->connect_error);
} //add user ticket
$query = "INSERT INTO `tickets` VALUES(NULL,".$_GET['q'].",".$_SESSION['uid'].",1,0)";
$result = mysqli_query($con, $query) or die("Data not found.");
header("location: event_view_fnt.php?q=".$_GET['q']."&add=true");
?>