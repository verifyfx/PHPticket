<?php
/*VERSION REPORT STATUS*/
// Start session
session_start();
require_once('./controlpanel/includes/functions.inc.php');
?>
<?php //check for log in session; otherwise tell to log in
    if(isset($_SESSION['logged_in'])) {
        include "./controlpanel/includes/config.inc.php";
        $con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
        if($con->connect_error){
            die("Connection failed: ".$con->connect_error);
        }
        $ctime = time();
        $query = "SELECT `events`.`eventID`, `events`.`eventName`, `events`.`eventStart`, `events`.`ticketPrice`, `events`.`location`, `events`.`eventAdmin`, `events`.`registerClosed`, `tickets`.`ticketID` FROM `events` INNER JOIN `tickets` ON `events`.`eventID` = `tickets`.`forEvent` INNER JOIN `users` ON `tickets`.`owner` = `users`.`userID` WHERE `users`.`userID`='".$_SESSION['uid']."'";
        if($_SESSION['admin']) {
            $query = "SELECT `events`.`eventID`, `events`.`eventName`, `events`.`eventStart`, `events`.`ticketPrice`, `events`.`location`, `events`.`eventAdmin`, `events`.`registerClosed`, `tickets`.`ticketID`, `users`.`username` FROM `events` INNER JOIN `tickets` ON `events`.`eventID` = `tickets`.`forEvent` INNER JOIN `users` ON `tickets`.`owner` = `users`.`userID`";
        }
        $result = mysqli_query($con, $query) or die("Data not found.x");
        $data = array();
        while($row = mysqli_fetch_assoc($result)) { //get data
            $row['eventStart'] = date('d/m/y', $row['eventStart']);
            $query = "SELECT `username` FROM `users` WHERE userID = '".$row['eventAdmin']."'";
            $result2 = mysqli_query($con, $query) or die("Data not found.");
            $result2 = mysqli_fetch_array($result2);
            $row['eventAdmin'] = $result2['username'];
            $data[] = $row;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Ticket Now</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        <!-- DataTables CSS -->
    <link href="controlpanel/js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="controlpanel/js/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <?php include 'header.php'; constructHeader(__FILE__); ?>
    <div class="container"><?php if(isset($_SESSION['logged_in'])) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>My Ticket List</h2>
                        <?php if(isset($_GET['del'])) { ?><h3><font color="red">Suscessfully deleted ticket</font></h3><?php } ?>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Event ID</th>
                                        <th>Event Name</th>
                                        <th>Event Day</th>
                                        <th>Ticket Price</th>
                                        <th>Location</th>
                                        <th>Admin</th>
                                        <th>Ticket ID</th>
                                        <?php if($_SESSION['admin']) { ?><th>Ticket Owner</th><?php } ?>
                                        <th>Cancle</th>
                                    </tr>
                                </thead>
                                <tbody>
                               <?php 
                                $curr = 0;
                                foreach ($data as $row) {
                                    echo '<tr>';
                                    echo '<td>' . $row['eventID'] . '</td>';
                                    ?><td><a href="event_view_fnt.php?q=<?php echo $row['eventID']; ?>&tid=<?php echo $row['ticketID']; ?>"><?php echo $row['eventName']; ?></a></td> <?php
                                    echo '<td>' . $row['eventStart'] . '</td>';
                                    echo '<td>' . $row['ticketPrice'] . '</td>';
                                    echo '<td>' . $row['location'] . '</td>';
                                    echo '<td>' . $row['eventAdmin'] . '</td>';
                                    echo '<td>' . $row['ticketID'] . '</td>';
                                    if($_SESSION['admin']) {
                                        echo '<td>' . $row['username'] . '</td>';
                                    }
                                    if($row['registerClosed']>$ctime || $_SESSION['admin']) {
                                        echo '<td align="center"><a href="event_del_fnt.php?q='. $row['eventID'] .'&tid='. $row['ticketID'] .'">' . '<i class="glyphicon glyphicon-remove-sign"></i>' . '</a></td>';
                                    } else {
                                        echo '<td>' . " " . '</td>';
                                    }
                                    echo "</tr>";
                                    }
                                 ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
            <h2>Please note that you can't cancle ticket after the registration has been closed</h2><br><br><br><br><br>
        </div>
        <!-- /.row -->
        <?php } else {
            include 'not_logged_in.php';
        } ?>
    </div>
    
    <?php include('footer.php'); ?>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
    <script src="controlpanel/js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="controlpanel/js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
</body>
</html>
