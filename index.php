<?php
/*VERSION REPORT STATUS*/
// Start session
session_start();
require_once('./controlpanel/includes/functions.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Ticket Now</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
</head><!--/head-->

<body>
    <?php include 'header.php'; constructHeader(__FILE__); ?>

    <section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                        <h1>Enjoy seemless event booking</h1>
                        <p>Life just got awesome, join events!</p>
                        <a href="broweevent.php" class="btn btn-common">Search for event</a>
                    </div>
                    <img src="images/home/slider/hill.png" class="slider-hill" alt="slider image">
                    <img src="images/home/clients.png" class="slider-house" alt="slider image">
                    <img src="images/home/slider/birds1.png" class="slider-birds1" alt="slider image">
                    <img src="images/home/slider/birds2.png" class="slider-birds2" alt="slider image">
                </div>
            </div>
        </div>
        <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
    </section>
    <!--/#home-slider-->

    <section id="services">
        <div class="container">
            <div class="row">
            <h2>
                Random events
            </h2>
            </div>
            <div class="row" ng-app="myApp" ng-controller="myCtrl">
                <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms" ng-repeat="x in myData track by $index">
                    <div class="single-service">
<!--                         <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
    <img src="images/home/icon1.png" alt="">
</div> -->
                        <h2><a href="event_view_fnt.php?q={{x.eventID}}">{{x.eventName}}</a></h2>
                        <p>Price: <strong>{{x.ticketPrice}}</strong> Location: <strong>{{x.location}}</strong></p>
                    </div>
                </div>
            </div>
        </div>
            <script type="text/javascript">
                var app = angular.module('myApp', []);
                app.controller('myCtrl', function($scope, $http) {
                    $http.get("randomevents.php").then(function(response) {
                        $scope.myData = response.data;
                    });
                });
            </script>
    </section>
    <!--/#services-->

    <?php if(!isset($_SESSION['logged_in'])) {
        ?>    <section id="action" class="responsive">
        <div class="vertical-center">
             <div class="container">
                <div class="row">
                    <div class="action take-tour">
                        <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                            <h1 class="title">Register or Log in to get started</h1>
                            <p>It just take a few seconds to get started.</p>
                        </div>
                        <div class="col-sm-5 text-center wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                            <div class="tour-button">
                                <a href="./controlpanel/register.php" class="btn btn-common">Go to Account page</a>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </section><?php
    }
    ?>
    <!--/#action-->

    <?php include('footer.php'); ?>

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
