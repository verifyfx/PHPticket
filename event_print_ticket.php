<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('controlpanel/includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php //PDF generation
	require('controlpanel/includes/fpdf.php');
	class PDF extends FPDF {
		function Header() {
			// Logo
			$this->Image('images/logo.png',8,10,50,15);
			// Arial bold 15
			$this->SetFont('Arial','B',15);
			$this->Ln(5);
			// Move to the right
			$this->Cell(80);
			// Title
			$this->Cell(30,10,'Ticket Reciept',0,0,'C');
			// Line break
			$this->Ln(15);
			$this->Cell(190,0,'',1,0,'C');
			$this->Ln(10);


		}

		function generateContent($tid) {
			$this->SetFillColor(224,235,255); //content bg
			$this->SetTextColor(0,0,0); //content text
			$this->SetFont('');
			$data = $this->getSQLData($tid);
			$data[3] = date('d/m/y', $data[3]);
			$ln = array();
			$ln[] = "Event ID: ";
			$ln[] = "Event Name: ";
			$ln[] = "Ticket Price: ";
			$ln[] = "Event Day: ";
			$ln[] = "Event Location: ";
			$ln[] = "Ticket ID: ";
			$ln[] = "Ticket For: ";
			$ln[] = "E mail: ";
			for($i=0;$i<8;$i++) {
				$ln[$i] = $ln[$i].$data[$i];
				$this->Cell(90,6,$ln[$i],'',0,'L');
				$this->Ln();
			}
		}

		function getSQLData($tid) {
			include "controlpanel/includes/config.inc.php";
			$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
			if($con->connect_error){
				die("Connection failed: ".$con->connect_error);
			}
			$query = "SELECT `events`.`eventID`, `events`.`eventName`, `events`.`ticketPrice`, `events`.`eventStart`,`events`.`location`, `tickets`.`ticketID` , `users`.`username`, `users`.`email` FROM `events` INNER JOIN `tickets` ON `events`.`eventID`=`tickets`.`forEvent` INNER JOIN `users` ON `tickets`.`owner`=`users`.`userID` WHERE `tickets`.`ticketID`=".$tid;
			$result = mysqli_query($con, $query) or die("Data not found.");
			$result = mysqli_fetch_array($result,MYSQLI_NUM);
			return $result;
		}

		function userDetail() {
			$sUSR = "Printed by: ".$_SESSION['username'];
			$this->SetFillColor(255,165,153);
			$this->Ln(20);
			$this->Cell(20);
			$this->Cell(60,6,$sUSR,0,0,'L',true);
		}
	}


	//////////////////////////////////////////////////////////
	$pdf = new PDF();
	$pdf->SetFont('Arial','',12);
	$pdf->AddPage();
	$pdf->generateContent($_GET['tid']);
	$pdf->userDetail();
	$pdf->Output();
?>