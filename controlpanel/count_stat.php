<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>

<?php //count stat and return in json
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
    $query_u = "SELECT userID FROM users";
    $query_e = "SELECT eventID, eventStart FROM events";
    $query_t = "SELECT ticketID FROM tickets";
    $result_u = mysqli_query($con, $query_u) or die("Data not found.");
    $result_e = mysqli_query($con, $query_e) or die("Data not found.");
    $result_t = mysqli_query($con, $query_t) or die("Data not found.");

	header("Content-Type: application/json");

	$count_u = mysqli_num_rows($result_u);
	$count_e = mysqli_num_rows($result_e);
	$count_e_fin = 0;
	$count_t = mysqli_num_rows($result_t);

    while($row = mysqli_fetch_assoc($result_e)) {
        if($row['eventStart']<time()) $count_e_fin++;
    }

	$data['users'] = $count_u;
	$data['events'] = $count_e;
	$data['events_fin'] = $count_e_fin;
	$data['tickets'] = $count_t;

	echo json_encode($data);
?>