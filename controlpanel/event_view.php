<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	if(!isset($_GET['evtid'])) header("location: eventlist.php");
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$query = "SELECT * FROM `events` WHERE eventID = '".$_GET['evtid']."'";
	$result = mysqli_query($con, $query) or die("Data not found.");
	$result = mysqli_fetch_array($result);
	$result['createdOn'] = date('d/m/y', $result['createdOn']);
	$result['registerClosed'] = date('d/m/y', $result['registerClosed']);
	$result['eventStart'] = date('d/m/y', $result['eventStart']);

	$query = "SELECT `username` FROM `users` WHERE userID = '".$result['eventAdmin']."'";
	$result2 = mysqli_query($con, $query) or die("Data not found.");
	$result2 = mysqli_fetch_array($result2);

	$query = "SELECT `tickets`.`ticketID`, `tickets`.`forEvent`, `users`.`username`, `tickets`.`valid`, `tickets`.`used` FROM `tickets` INNER JOIN `users` ON `tickets`.`owner`=`users`.`userID` WHERE `tickets`.`forEvent`=".$_GET['evtid'];
	$result3 = mysqli_query($con, $query) or die("Data not found.");
?><!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Event View</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
    <!-- DataTables CSS -->
    <link href="js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="js/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
		
		
		
</head>

<body>
		<?php include 'header.php'; ?>
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<?php include 'sidebar.php'; ?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			<div class="row-fluid">
				<?php if(isset($_GET['new'])) {
					?><h3>Congraturations! your event has been created.</h3>
				<h4>here's the information</h4><?php
				} else {
					?><h3>Event Details</h3><?php
				}
				?>
				<div class="col-lg-8">
					<p>Event ID: <span><strong><?php echo $result['eventID']; ?></strong></span></p>
					<p>Name: <span><strong><?php echo $result['eventName']; ?></strong></span></p>
					<p>Description: <span><strong><?php echo $result['eventDesc']; ?></strong></span></p>
					<p>Creation Time: <span><strong><?php echo $result['createdOn']; ?></strong></span></p>
					<p>Register Closed: <span><strong><?php echo $result['registerClosed']; ?></strong></span></p>
					<p>Event Day: <span><strong><?php echo $result['eventStart']; ?></strong></span></p>
					<p>Capacity: <span><strong><?php echo $result['capacity']; ?></strong></span></p>
					<p>Ticket Price: <span><strong><?php echo $result['ticketPrice']; ?></strong></span></p>
					<p>Event Admin: <span><strong><?php echo $result2['username']; ?></strong></span></p>
				</div>
			</div>
			<div class="row-fluid"><BR>
				<h2>People who joined your events</h2>
				<div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" align="right" width="97%" id="dataTables-example">
                        <thead>
                            <tr>
                                <th width="10%">TicketID</th>
                                <th width="20%">Username</th>
                                <th width="10%">Valid Status</th>
                                <th width="10%">Used Status</th>
                            </tr>
                        </thead>
                        <tbody>
                       <?php 
                        while($row = mysqli_fetch_array($result3)) {
                        	?>
                        	<tr>
                        		<td><?php echo $row['ticketID']; ?></td>
                        		<td><?php echo $row['username']; ?></td>
                        		<td><?php if($row['valid']){ ?><i class="glyphicons-icon usd"></i><?php } ?></td>
                        		<td><?php if($row['used']){ ?><i class="glyphicons-icon certificate"></i><?php } ?></td>
                        	</tr>
                        	<?php
                        }
                       ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
			</div>	

			<div class="row-fluid">
				<a href="event_excel.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicons-icon	list"></i>Print (Excel) participants list</a><br>
				<a href="event_manage.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicons-icon settings"></i>Edit this event</a><br>
				<a href="event_delete.php?q=<?php echo $result['eventID']; ?>"><i class="glyphicons-icon remove"></i><font color="RED">Delete this event</font></a><br>
				<a href="eventlist.php"><i class="glyphicons-icon book"></i>Go to event list</a>
			</div>	
	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	
	<div class="clearfix"></div>
	
	<?php include 'footer.php'; ?>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>

		<script src="js/datatables/media/js/jquery.dataTables.min.js"></script>
    	<script src="js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	    <script>
	    $(document).ready(function() {
	        $('#dataTables-example').DataTable({
	                responsive: true
	        });
	    });
	    </script>
		<!-- end: JavaScript-->
	
</body>
</html>
