<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	if(!isset($_POST['username'])) {
		if($_POST['username']=="") {
			header("location: user_manage.php?q=".$_POST['userid']."&err=blankusr");
		}
		header("location: user_manage.php?q=".$_POST['userid']."&err=blankusr");
	}
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$userxname = mysqli_real_escape_string($con,$_POST['username']);
	$userxpassword = mysqli_real_escape_string($con,$_POST['password']);
	$userxemail = mysqli_real_escape_string($con,$_POST['email']);
	if($_SESSION['admin']) {
		$userxlvl = mysqli_real_escape_string($con,$_POST['lvl']);
	} else {
		$userxlvl = "0";
	}
	$userxid = $_POST['userid'];
	$query = "";
	if($userxpassword=="") {
		$query = "UPDATE `users` SET `username`='".$userxname."', `level`='".$userxlvl."', `email`='".$userxemail."' WHERE userID = '".$userxid."'";
	} else {
		$userxpassword = md5($userxpassword);
		//password changes
		$query = "UPDATE `users` SET `username`='".$userxname."', `level`='".$userxlvl."', `password`='".$userxpassword."', `email`='".$userxemail."' WHERE userID = '".$userxid."'";
	}
	$result = mysqli_query($con, $query) or die("Data not found.");
	header("location: profile.php?q=".$userxid);
?>