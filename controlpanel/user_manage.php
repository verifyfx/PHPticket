<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
$err="";
//TODO: err check
if(!isset($_GET['q'])) header("location: userlist.php");

include "includes/config.inc.php";
$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
if($con->connect_error){
	die("Connection failed: ".$con->connect_error);
}
$query = "SELECT * FROM `users` WHERE userID = '".$_GET['q']."'";
$result = mysqli_query($con, $query) or die("Data not found.a");
$result = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Edit User</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
		
		
</head>

<body>
	<script type="text/javascript">
		function doValid() {
			if(document.getElementById("password").value == document.getElementById("password2").value) {
				document.getElementById("go").disabled = true;
				document.getElementById("username").disabled = false;
				document.getElementById("userman").submit();
			} else {
				document.getElementById("stat").innerHTML = "Password mismatch";
			}
		}
	</script>
		<?php include 'header.php'; ?>
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<?php include 'sidebar.php'; ?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			<div class="row-fluid">
				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header" data-original-title>
							<h2><i class="halflings-icon white edit"></i><span class="break"></span>Edit User</h2>
						</div>
						<div class="box-content">
							<form class="form-horizontal" action="edit_user_post.php" method="POST" name="userman" id="userman">
								<p class="help-block"><span class="label label-important" id="stat"><?php echo($err); ?></span></p>
								<fieldset>
								<input type="hidden" name="userid" value="<?php echo $_GET['q']; ?>" />
								  	<div class="control-group">
										<label class="control-label" for="username">Username</label>
										<div class="controls">
											<div class="input-prepend">
												<input id="username" name="username" type="text" disabled required value="<?php echo $result['username']; ?>" />
									  		</div>
										</div>
 								  	</div>
									<div class="control-group">
										<label class="control-label" for="password">Password</label>
										<div class="controls">
											<div class="input-prepend">
												<input id="password" name="password" type="password" />
									  		</div><span class="help-inline">Leave it blank if you don't want to change the password.</span>
										</div>
 								  	</div>
									<div class="control-group">
										<label class="control-label" for="password2">Retype Password</label>
										<div class="controls">
											<div class="input-prepend">
												<input id="password2" name="password2" type="password" />
									  		</div>
										</div>
 								  	</div>
 								  	<div class="control-group">
										<label class="control-label" for="email">E-mail</label>
										<div class="controls">
											<div class="input-prepend">
												<input id="email" name="email" type="email" required value="<?php echo $result['email']; ?>" />
									  		</div>
										</div>
 								  	</div>
 								  	<?php if($_SESSION['admin']) { ?>
									<div class="control-group">
										<label class="control-label" for="userID">Level</label>
										<div class="controls">
											<div class="input-prepend">
			                                    <select name="lvl" id="lvl">
			                                    <option value="0">user</option>
			                                    <option value="1" <?php if($result['level']) echo 'selected="selected"'; ?>>admin</option>
			                                    </select>
									  		</div>
										</div>
 								  	</div>
 								  	<?php } ?>
								  	<div class="form-actions">
										<button type="button" name="go" id="go" class="btn btn-primary" onclick="doValid()">Save user</button>
										<a href="userlist.php" class="btn" type="button">Cancel</a>
								  	</div>
								</fieldset>
							</form>
						</div>
					</div><!--/span-->

				</div><!--/row-->
			</div>		
			
			
       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	
	<div class="clearfix"></div>
	
	<?php include 'footer.php'; ?>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
