<?php
// Include required MySQL configuration file and functions  

require_once('config.inc.php');  
require_once('functions.inc.php');     		
require 'vendor/autoload.php';
use Mailgun\Mailgun;

// Start session  
session_start();     

// Check if user is already logged in  
if ($_SESSION['logged_in'] == true) {
	// If user is already logged in, redirect to main page                
	redirect('../index.php');
} else {
	// Make sure that user submitted a username/password and username only consists of alphanumeric chars
	if ((!isset($_POST['username'])) || (!isset($_POST['password'])) OR (!ctype_alnum($_POST['username'])) ) {
		redirect('../register.php?err=invalid');
	}
	// Connect to database
	$mysqli = @new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

	// Check connection
	if (mysqli_connect_errno()) {
		printf("Unable to connect to database: %s", mysqli_connect_error());
		exit();
	} 

	// Escape any unsafe characters before querying database                
	$username = $mysqli->real_escape_string($_POST['username']);                
	$password = $mysqli->real_escape_string($_POST['password']);                   
	$email    = $mysqli->real_escape_string($_POST['email']);

	$sql = "SELECT * FROM users WHERE username = '" . $username . "'";
	$result = $mysqli->query($sql);         
	            
	if (is_object($result) && $result->num_rows == 1) {                              
		redirect('../register.php?err=alreadytaken&usr='.$username);              
	} else {                                                         
		$_SESSION['user'] = $username;
		// Construct SQL statement for query & execute                
		$sql = "INSERT INTO `users` (`userID`, `username`, `password`, `email`, `level`) VALUES (NULL, '" . $username . "', '" . md5($password) . "', '". $email ."', '0')";
		$result2 = $mysqli->query($sql);
		$_SESSION['logged_in'] = true;  
		$_SESSION['uid']       = $mysqli->insert_id;
		$_SESSION['username']  = $username;
		$_SESSION['admin']     = 0;

		/////Mailgun integration
		$mg = new Mailgun("key-b5d4fdeac6609b5295d8a35b8dfb54dd");
		$domain = "vector.in.th";
		$mg->sendMessage($domain, array(
			'from'    => 'ticket-noreply@vector.in.th',
			'to'	  => $email,
			'subject' => 'You have suscessfully registered for Ticket System',
			'html'    => 'Please refer to the following credentials<BR>username: '.$username.'<BR>Password: '."<i>[Not shown due to security reason]</i>".'<BR>You can view and fully use our website at: <a href="http://vector.in.th/ticket">http://vector.in.th/ticket/</a><BR>Thank you for chosing us'
			)
		);
		redirect('../index.php');  
	}
}
?>