<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Bangkok');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("vector.in.th")
							 ->setLastModifiedBy("init.d")
							 ->setTitle("Generated Report")
							 ->setSubject("Participants report xlsx")
							 ->setDescription("Automated ticket web")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("result file");

	if(!isset($_GET['q'])) header("location: eventlist.php");
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$query = "SELECT * FROM `events` WHERE eventID = '".$_GET['q']."'";
	$result = mysqli_query($con, $query) or die("Data not found.");
	$result = mysqli_fetch_array($result);
	$result['createdOn'] = date('d/m/y', $result['createdOn']);
	$result['registerClosed'] = date('d/m/y', $result['registerClosed']);
	$result['eventStart'] = date('d/m/y', $result['eventStart']);

	$query = "SELECT `username` FROM `users` WHERE userID = '".$result['eventAdmin']."'";
	$result2 = mysqli_query($con, $query) or die("Data not found.");
	$result2 = mysqli_fetch_array($result2);

	$query = "SELECT `tickets`.`ticketID`, `tickets`.`forEvent`, `users`.`username`, `tickets`.`valid`, `tickets`.`used` FROM `tickets` INNER JOIN `users` ON `tickets`.`owner`=`users`.`userID` WHERE `tickets`.`forEvent`=".$_GET['q'];
	$result3 = mysqli_query($con, $query) or die("Data not found.");

$posX = 'A';
$posY = 1;
// Add some data
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, 'TicketID');
$posX++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, 'Username');
$posX++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, 'Vaildity');
$posX++;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, 'Used?');
$posX++;
$posY=2;
$posX = 'A';
	while ($row = mysqli_fetch_array($result3)) {
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, $row['ticketID']);
		$posX++;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, $row['username']);
		$posX++;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, $row['valid']);
		$posX++;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($posX.$posY, $row['used']);
		$posX++;
		$posX = 'A';
		$posY++;
	}


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
//////////////////////////////////////////////////////////////////////////
?>