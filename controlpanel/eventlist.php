<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Event List</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		
		
		
</head>

<body>
		<?php include 'header.php'; ?>
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<?php include 'sidebar.php'; ?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			<div class="row-fluid">
				<h3>Event List &amp; Management</h3>
				<table class="table table-hover table-striped" ng-app="myApp" ng-controller="myCtrl">
			    <thead>
			      <tr>
			        <th>Event ID</th>
			        <th>Event Name</th>
			        <th>Created On</th>
			        <th>Registration Close</th>
			        <th>Event Start</th>
					<th>Joined</th>
					<th>Capacity</th>
					<th>Ticket Price</th>
					<th>Event Admin</th>
					<th>Manage</th>
					<th>Delete Event</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr ng-repeat="x in myData">
			        <td>{{x.eventID}}</td>
			        <td><a href="event_view.php?evtid={{x.eventID}}">{{x.eventName}}</a></td>
			        <td>{{x.createdOn}}</td>
			        <td>{{x.registerClosed}}</td>
			        <td>{{x.eventStart}}</td>
			        <td>{{x.joined}}</td>
			        <td>{{x.capacity}}</td>
			        <td>{{x.ticketPrice}}</td>
			        <td>{{x.eventAdmin}}</td>
			        <td><a href="event_manage.php?q={{x.eventID}}"><i class="glyphicons-icon settings"></i></a></td>
			        <td><a href="event_delete.php?q={{x.eventID}}"><i class=" glyphicons-icon remove"></i></a></td>
			      </tr>
			    </tbody>
			  </table>
			  <script type="text/javascript">
	            var app = angular.module('myApp', []);
	            app.controller('myCtrl', function($scope, $http) {
	                $http.get("eventlist_qry.php").then(function(response) {
	                    $scope.myData = response.data;
	                });
	            });
            </script>
			</div>		
			
			
       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	
	<div class="clearfix"></div>
	
	<?php include 'footer.php'; ?>

	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
