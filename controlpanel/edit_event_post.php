<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	if(!isset($_POST['eventName'])) {
		if($_POST['eventName']=="") {
			header("location: eventlist.php");
		}
		header("location: eventlist.php");
	}
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	//http://stackoverflow.com/questions/15380599/how-to-convert-a-date-yyyy-mm-dd-to-epoch-in-php
	$ename = mysqli_real_escape_string($con,$_POST['eventName']);
	$edesc = mysqli_real_escape_string($con,$_POST['eventDesc']);
	$created = $_POST['createdOn'];
	//die($_POST['last_regis']);
	$regclosed = DateTime::createFromFormat('m/d/Y', $_POST['last_regis']);
	//die(print_r($regclosed));
	$regclosed = date_format($regclosed, 'U');
	//die($regclosed);
	$estart = DateTime::createFromFormat('m/d/Y', $_POST['event_day']);
	$estart = date_format($estart, 'U');
	$ecap = mysqli_real_escape_string($con,$_POST['eventCap']);
	$price = mysqli_real_escape_string($con,$_POST['price']);
	$location = mysqli_real_escape_string($con, $_POST['location']);
	if($created>$regclosed) {
		header("location: event_manage.php?q=".$_POST['evtID']."&err=regclosedb4created");
	}
	if($created>$estart) {
		header("location: event_manage.php?q=".$_POST['evtID']."&err=eventstartb4created");
	}
	if($regclosed>$estart) {
		header("location: event_manage.php?q=".$_POST['evtID']."&err=eventstartb4regclosed");
	}
	$query = "UPDATE `events` SET `eventName`='".$ename."', `eventDesc`='".$edesc."', `createdOn`='".$created."', `registerClosed`='".$regclosed."', `eventStart`='".$estart."', `capacity`='".$ecap."', `ticketPrice`='".$price."', `location`='".$location."' WHERE eventID='".$_POST['evtID']."'";
	$result = mysqli_query($con, $query) or die("Data not found.");
	header("location: event_view.php?evtid=".$_POST['evtID']);
?>