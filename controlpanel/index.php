<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Controlpanel Home</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		
		
		
</head>

<body>
		<?php include 'header.php'; ?>
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<?php include 'sidebar.php'; ?>
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			<div class="row-fluid">	
				<h2>Welcome to ticket management services</h2>
			</div><!--/row-->
			
			<?php if($_SESSION['admin']) { ?>
			<div class="row-fluid" ng-app="myApp" ng-controller="myCtrl">
				
				<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
					<div class="boxchart">5,6,7,2,0,4,2,4,8,2,3,3,2</div>
					<div class="number">{{myData.users}}</div>
					<div class="title">Users</div>
				</div>
				<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
					<div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
					<div class="number">{{myData.events}}</div>
					<div class="title">Events</div>
				</div>
				<div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
					<div class="boxchart">5,6,7,2,5,4,6,4,8,2,3,3,2</div>
					<div class="number">{{myData.events_fin}}</div>
					<div class="title">Finished events</div>
				</div>
				<div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
					<div class="boxchart">7,2,2,2,1,5,9,4,8,3,4,3,3,5</div>
					<div class="number">{{myData.tickets}}</div>
					<div class="title">ticket created</div>
				</div>	
				
			</div>
            <script type="text/javascript">
                        var app = angular.module('myApp', []);
                        app.controller('myCtrl', function($scope, $http) {
                            $http.get("count_stat.php").then(function(response) {
                                $scope.myData = response.data;
                            });
                        });
            </script>	
			<?php } ?>	

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	
	<div class="clearfix"></div>
	
	<?php include 'footer.php'; ?>
	
</body>
</html>
