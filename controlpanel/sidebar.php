			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="index.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard Home</span></a></li> <!-- all -->
						<li><a href="eventlist.php"><i class="icon-book"></i><span class="hidden-tablet"> Event List</span></a></li> <!-- all -->
						<li><a href="createevent.php"><i class="icon-calendar"></i><span class="hidden-tablet"> Create Event</span></a></li> <!-- event mananger+ -->
						<?php if($_SESSION['admin']) { ?><li><a href="userlist.php"><i class="icon-user"></i><span class="hidden-tablet"> User List</span></a></li><?php } ?> <!-- admin -->
						<li><a href="logout.php"><i class="icon-lock"></i><span class="hidden-tablet"> Logout</span></a></li> <!-- all -->
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->