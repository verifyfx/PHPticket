<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php //put event to db
	if(!isset($_POST['eventName'])) {
		header("location: createevent.php");
	}
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	$ename = mysqli_real_escape_string($con,$_POST['eventName']);
	$edesc = mysqli_real_escape_string($con,$_POST['eventDesc']);
	$created = time();
	$regclosed = DateTime::createFromFormat('m/d/Y', $_POST['last_regis']);
	$regclosed = date_format($regclosed, 'U');
	$estart = DateTime::createFromFormat('m/d/Y', $_POST['event_day']);
	$estart = date_format($estart, 'U');
	$ecap = mysqli_real_escape_string($con,$_POST['eventCap']);
	$price = mysqli_real_escape_string($con,$_POST['price']);
	$location = mysqli_real_escape_string($con, $_POST['location']);
	//////////////
	//echo "c:".$created."<BR>r:".$regclosed."<BR>s:".$estart;
	if($created>$regclosed) {
		header("location: createevent.php?err=regclosedb4created");
	}
	if($created>$estart) {
		header("location: createevent.php?err=eventstartb4created");
	}
	if($regclosed>$estart) {
		header("location: createevent.php?err=eventstartb4regclosed");
	}
	//die("<BR>EOF");
	$query = "INSERT INTO `events` (`eventID`, `eventName`, `eventDesc`, `createdOn`, `registerClosed`, `eventStart`, `joined`, `capacity`, `eventAdmin`, `ticketPrice`, `location`) VALUES (NULL, '" . $ename . "', '" . $edesc . "', '" . $created . "', '" . $regclosed . "', '" . $estart . "', '0', '" . $ecap . "', '" . $_SESSION['uid'] . "', '" . $price . "', '". $location ."')";
	$result = mysqli_query($con, $query) or die("Data not found.");
	$last_id = mysqli_insert_id($con);
	header("location: event_view.php?evtid=".$last_id."&new=true");
?>