<?php
/*
      __                     __           ___                                  
     /\ \                   /\ \         /\_ \                   __            
  ___\ \ \___      __    ___\ \ \/'\     \//\ \     ___      __ /\_\    ___    
 /'___\ \  _ `\  /'__`\ /'___\ \ , <       \ \ \   / __`\  /'_ `\/\ \ /' _ `\  
/\ \__/\ \ \ \ \/\  __//\ \__/\ \ \\`\      \_\ \_/\ \L\ \/\ \L\ \ \ \/\ \/\ \ 
\ \____\\ \_\ \_\ \____\ \____\\ \_\ \_\    /\____\ \____/\ \____ \ \_\ \_\ \_\
 \/____/ \/_/\/_/\/____/\/____/ \/_/\/_/    \/____/\/___/  \/___L\ \/_/\/_/\/_/
                                                             /\____/           
                                                             \_/__/            
*/
//http://www.network-science.de/ascii/
// Start session
session_start();
// Include required functions file
require_once('includes/functions.inc.php');
// Check login status... if not logged in, redirect to login screen
if (check_login_status() == false) {
    redirect('login.php');
    }
?>
<?php
	include "includes/config.inc.php";
	$con = new mysqli(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	if($con->connect_error){
		die("Connection failed: ".$con->connect_error);
	}
	if($_SESSION['admin']) {
		$query = "SELECT * FROM events";
	} else {
		$query = "SELECT * FROM events WHERE eventAdmin='".$_SESSION['uid']."'";
	}
	$result = mysqli_query($con, $query) or die("Data not found.");
	header("Content-Type: application/json");
	$data = array();
    while($row = mysqli_fetch_assoc($result)) {
    	$row['createdOn'] = date('d/m/y', $row['createdOn']);
    	$row['registerClosed'] = date('d/m/y', $row['registerClosed']);
    	$row['eventStart'] = date('d/m/y', $row['eventStart']);
        $query = "SELECT `username` FROM `users` WHERE userID = '".$row['eventAdmin']."'";
        $result2 = mysqli_query($con, $query) or die("Data not found.");
        $result2 = mysqli_fetch_array($result2);
        $row['eventAdmin'] = $result2['username'];
        $query = "SELECT COUNT(`forEvent`) AS joined FROM tickets WHERE forEvent=".$row['eventID'];
        $result3 = mysqli_query($con, $query) or die("Data not found2.");
        $result3 = mysqli_fetch_array($result3);
      $row['joined'] = $result3['joined'];
        $data[] = $row;
    }
    echo json_encode($data);
?>